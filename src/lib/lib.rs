//┌────────────────────────────────────────────────────────────────────────────┐
//│ PasteIt - A paste client                                                   │
//│ ========================                                                   │
//│ File   : pasteit/src/lib/lib.rs                                            │
//│ License: Mozilla Public License 2.0                                        │
//│ URL    : https://gitlab.com/shivanandvp/pasteit                            │
//│ Authors:                                                                   │
//│     1. shivanandvp <shivanandvp@rebornos.org>                              │
//│     2.                                                                     │
//│ -----                                                                      │
//│ Description:                                                               │
//│                                                                            │
//│                                                                            │
//│ -----                                                                      │
//│ Last Modified: Sat, 8th January 2022 4:20:05 PM -06:00                     │
//│ Modified By  :                                                             │
//│ -----                                                                      │
//│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>               │
//│ 2.                                                                         │
//└────────────────────────────────────────────────────────────────────────────┘

// region: API FACADE and IMPORTS

// Declares and re-exports the existence of inner modules in other files
pub mod dpaste;
pub mod privatebin;

// Imports and publicly re-exports necessary items for use from outside this module
pub use data_model::*;
pub use error::*;
pub use interface::*;

// endregion: API FACADE and IMPORTS

// region: MODULES

mod error {
    // region: API FACADE and IMPORTS

    // Privately imports for use in this module
    use snafu::Snafu;

    // endregion: API FACADE and IMPORTS

    #[derive(Debug, Snafu)]
    #[snafu(visibility(pub))]
    pub enum Error {
        #[snafu(display(
            "Unable to compile the below Regex pattern for parsing expiry duration: {}\n{}",
            regex_pattern,
            source
        ))]
        DurationRegexError {
            regex_pattern: String,
            source: regex::Error,
        },

        #[snafu(display(
    "While parsing the given expiry time, the current system time was found to be earlier than the UNIX epoch (far in the past): {}",
    source
))]
        PastSystemTimeError { source: std::time::SystemTimeError },

        #[snafu(display(
    "While parsing the given expiry time, the current system time was found to be far in the future: {}",
    source
))]
        FutureSystemTimeError { source: std::num::TryFromIntError },

        #[snafu(display(
    "The provided expiry duration is either invalid, or rounded to {}s and is too small. Please ensure that it is at least 1 min", duration_in_seconds
))]
        ExpiryFormatError { duration_in_seconds: i64 },

        #[snafu(display(
    "The provided value \"{}\" for the parameter \"{}\" is invalid. The supported values are: {:?}", value , parameter_name , supported_values
))]
        EnumParameterFormatError {
            value: String,
            parameter_name: String,
            supported_values: Vec<String>,
        },

        #[snafu(display(
    "The provided value \"{}\" for the parameter \"{}\" is invalid. The correct format is: {}", value , parameter_name , supported_format
))]
        StringParameterFormatError {
            value: String,
            parameter_name: String,
            supported_format: String,
        },

        #[snafu(display("{} task failed : {}", task_name, message))]
        WebApiError { task_name: String, message: String },

        #[snafu(display("Unable to convert the data {} into JSON : {}", given_data, source))]
        JsonSerializeError {
            given_data: String,
            source: serde_json::error::Error,
        },

        #[snafu(display(
            "Unable to convert the JSON data {} into objects : {}",
            given_data,
            source
        ))]
        JsonDeserializeError {
            given_data: String,
            source: serde_json::error::Error,
        },

        #[snafu(display("Encountered an HTTP error while {} : {}", task_verb, source))]
        HttpError {
            task_verb: String,
            source: reqwest::Error,
        },

        #[snafu(display("Unable to convert the base64 input to bytes: {}", source))]
        Base64DecodeError { source: base64::DecodeError },
    }
}

mod data_model {
    // region: API FACADE AND IMPORTS

    // Privately imports for use in this module
    use crate::error::*;
    use serde::{Deserialize, Serialize};

    // endregion: API FACADE AND IMPORTS

    // region: DATA MODEL

    /// The Cipher algorithm. Note: Currently only AES is supported
    #[derive(Copy, Clone, Debug, Serialize, Deserialize)]
    pub enum CipherAlgorithm {
        Aes,
    }

    /// The Cipher mode. Note: Currently only GCM is supported
    #[derive(Copy, Clone, Debug, Serialize, Deserialize)]
    pub enum CipherMode {
        Gcm,
    }

    /// The Paste compression type. Note: Currently only Zlib and no compression are supported
    #[derive(Copy, Clone, Debug, Serialize, Deserialize)]
    pub enum CompressionType {
        Zlib,
        None,
    }

    /// The format in which the paste is displayed
    #[derive(Copy, Clone, Debug, Serialize, Deserialize)]
    pub enum PasteDisplayFormat {
        PlainText,
        SyntaxHighlighting,
        Markdown,
    }

    // endregion: DATA MODEL

    // region: CONVERSION TO String

    impl From<CipherAlgorithm> for String {
        fn from(given_enum: CipherAlgorithm) -> Self {
            match given_enum {
                CipherAlgorithm::Aes => "aes".into(),
            }
        }
    }

    impl From<CipherMode> for String {
        fn from(given_enum: CipherMode) -> Self {
            match given_enum {
                CipherMode::Gcm => "gcm".into(),
            }
        }
    }

    impl From<CompressionType> for String {
        fn from(given_enum: CompressionType) -> Self {
            match given_enum {
                CompressionType::Zlib => "zlib".into(),
                CompressionType::None => "none".into(),
            }
        }
    }

    impl From<PasteDisplayFormat> for String {
        fn from(given_enum: PasteDisplayFormat) -> Self {
            match given_enum {
                PasteDisplayFormat::PlainText => "plaintext".into(),
                PasteDisplayFormat::SyntaxHighlighting => "syntaxhighlighting".into(),
                PasteDisplayFormat::Markdown => "markdown".into(),
            }
        }
    }

    // endregion: CONVERSION TO String

    // region: CONVERSION FROM String

    impl TryFrom<String> for CipherAlgorithm {
        type Error = Error;
        fn try_from(given_string: String) -> Result<Self, Self::Error> {
            match given_string.to_lowercase().as_ref() {
                "aes" => Ok(CipherAlgorithm::Aes),
                _ => {
                    Err(Self::Error::EnumParameterFormatError {
                        value: given_string,
                        parameter_name: "Paste Cipher Algorithm".into(),
                        supported_values: vec!["aes".into()],
                    })
                }
            }
        }
    }

    impl TryFrom<String> for CipherMode {
        type Error = Error;
        fn try_from(given_string: String) -> Result<Self, Self::Error> {
            match given_string.to_lowercase().as_ref() {
                "gcm" => Ok(CipherMode::Gcm),
                _ => {
                    Err(Self::Error::EnumParameterFormatError {
                        value: given_string,
                        parameter_name: "Paste Cipher Mode".into(),
                        supported_values: vec!["gcm".into()],
                    })
                }
            }
        }
    }

    impl TryFrom<String> for CompressionType {
        type Error = Error;
        fn try_from(given_string: String) -> Result<Self, Self::Error> {
            match given_string.to_lowercase().as_ref() {
                "zlib" => Ok(CompressionType::Zlib),
                "none" => Ok(CompressionType::None),
                _ => {
                    Err(Self::Error::EnumParameterFormatError {
                        value: given_string,
                        parameter_name: "Paste Compression Type".into(),
                        supported_values: vec!["zlib".into(), "none".into()],
                    })
                }
            }
        }
    }

    impl TryFrom<String> for PasteDisplayFormat {
        type Error = Error;
        fn try_from(given_string: String) -> Result<Self, Self::Error> {
            match given_string.to_lowercase().as_ref() {
                "plaintext" => Ok(PasteDisplayFormat::PlainText),
                "syntaxhighlighting" => Ok(PasteDisplayFormat::SyntaxHighlighting),
                "markdown" => Ok(PasteDisplayFormat::Markdown),
                _ => {
                    Err(Self::Error::EnumParameterFormatError {
                        value: given_string,
                        parameter_name: "Paste Display Format".into(),
                        supported_values: vec![
                            "plaintext".into(),
                            "syntaxhighlighting".into(),
                            "markdown".into(),
                        ],
                    })
                }
            }
        }
    }

    // endregion: CONVERSION FROM String
}

mod interface {

    // region: API FACADE and IMPORTS

    // Privately imports for use in this module
    use crate::*;
    use serde::{Deserialize, Serialize};
    use url::Url;

    // endregion: API FACADE and IMPORTS

    /// The parameters that are needed for a paste upload
    #[derive(Serialize, Deserialize, Debug)]
    pub struct PasteInputInterface {
        pub paste_text: String,
        pub password: String,
        pub url: Url,
        pub id: Option<String>,
        pub expiry_duration: String,
        pub burn_after_reading: bool,
        pub open_discussion: bool,
        pub paste_display_format: PasteDisplayFormat,
        pub compression_type: CompressionType,
        pub cipher_algorithm: CipherAlgorithm,
        pub cipher_mode: CipherMode,
    }

    /// The parameters that are needed for a comment upload
    #[derive(Serialize, Deserialize, Debug)]
    pub struct CommentInputInterface {
        pub comment_text: String,
        pub password: String,
        pub url: Url,
        pub compression_type: super::CompressionType,
        pub cipher_algorithm: super::CipherAlgorithm,
        pub cipher_mode: super::CipherMode,
        pub parent_id: String,
        pub paste_id: String,
        pub nickname: String,
        pub decryption_key: String,
    }

    /// The parameters that are needed to delete a paste
    #[derive(Serialize, Deserialize, Debug)]
    pub struct DeletionInputInterface {
        pub paste_id: String,
        pub delete_token: String,
    }

    /// The parameters needed to access a paste
    #[derive(Serialize, Deserialize, Debug)]
    pub struct PasteOutputInterface {
        pub url: Url,
        pub paste_id: String,
        pub decryption_key: String,
        pub deletion_url: Url,        
        pub deletion_token: String,
    }

    /// The parameters needed to access a comment
    #[derive(Serialize, Deserialize, Debug)]
    pub struct CommentOutputInterface {
        pub url: Url,
        pub comment_id: String,
    }

    /// The parameters that are output after deletion
    #[derive(Serialize, Deserialize, Debug)]
    pub struct DeletionOutputInterface {}
}

// endregion: MODULES

